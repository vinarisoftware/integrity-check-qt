<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>AboutWindow</name>
    <message>
        <location filename="ui/aboutwindow.ui" line="29"/>
        <source>Integrity Check - Vinari Software | About</source>
        <translation>Integrity Check - Vinari Software | Acerca de</translation>
    </message>
    <message>
        <location filename="ui/aboutwindow.ui" line="52"/>
        <source>Integrity Check - Vinari Software is a simple utily available for Microsoft Windows, Vinari OS (And various Linux distributions)

It allows you to get the hashing sum of your files and also compare them, to garantee that the files you download from the Internet, or copy from external devices are not corrupted.</source>
        <translation>Integrity Check - Vinari Software es una simple utilidad disponible para Microsoft Windows, Vinari OS (Y varias distribuciones GNU/Linux)

Le permite calcular la suma de verificación de sus archivos y además comparar dichas sumas, para garantizar que los archivos que descarga de Internet, o transfiere mediante dispositivos externos estén intactos.</translation>
    </message>
    <message>
        <location filename="ui/aboutwindow.ui" line="75"/>
        <source>License agreement</source>
        <translation>Acuerdo de licencia</translation>
    </message>
    <message>
        <location filename="ui/aboutwindow.ui" line="82"/>
        <source>Accept</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location filename="ui/aboutwindow.ui" line="85"/>
        <source>Esc</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/aboutwindow.ui" line="112"/>
        <source>Integrity Check - Vinari Software</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/aboutwindow.ui" line="143"/>
        <source>Version: 2.2.0.2 
All rights reserved. All logos &amp; pictures here are property of Vinari Software
And of their respective owners.
All the source code and it&apos;s binary files are licensed under the BSD-3 license.</source>
        <oldsource>Version: 2.2.0.1 
All rights reserved. All logos &amp; pictures here are property of Vinari Software
And of their respective owners.
All the source code and it&apos;s binary files are licensed under the BSD-3 license.</oldsource>
        <translation>Versión: 2.2.0.2
Todos los derechos reservados. Todos los logos aquí son propiedad de Vinari Software y de sus respetivos dueños.
Todo el código fuente y los archivos binarios están protegidos bajo la licencia BSD-3.</translation>
    </message>
</context>
<context>
    <name>LicenseWindow</name>
    <message>
        <location filename="ui/licensewindow.ui" line="38"/>
        <source>Integrity Check - Vinari Software | License</source>
        <translation>Integrity Check - Vinari Software | Licencia</translation>
    </message>
    <message>
        <location filename="ui/licensewindow.ui" line="93"/>
        <source>Accept</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location filename="ui/licensewindow.ui" line="96"/>
        <source>Esc</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="ui/mainwindow.ui" line="29"/>
        <source>Integrity Check -  Vinari Software</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="79"/>
        <location filename="src/mainwindow.cpp" line="165"/>
        <source>Type the verification sum you got from the file:</source>
        <translation>Ingrese la suma de verificación que obtuvo del archivo:</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="94"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Copy this hash&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Copiar este hash&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="138"/>
        <location filename="src/mainwindow.cpp" line="167"/>
        <source>Type the verification sum of the file that should be:</source>
        <oldsource>Ingrese la suma de verificación que debe obtener:</oldsource>
        <translation>Ingrese la suma de verificación que debe obtener:</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="153"/>
        <source>Copy this hash</source>
        <translation>Copiar este hash</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="189"/>
        <source>Compare sums</source>
        <translation>Comparar sumas</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="196"/>
        <source>New comparison</source>
        <translation>Nueva comparación</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="199"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="217"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="226"/>
        <source>&amp;About</source>
        <translation>Acerca &amp;de</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="245"/>
        <source>Get hashing sum of a file</source>
        <translation>Obtener hash de un archivo</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="248"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="257"/>
        <source>Settings</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="260"/>
        <source>Ctrl+,</source>
        <translation>Ctrl+,</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="269"/>
        <source>About Integrity Check - Vinari Software</source>
        <oldsource>Acerca de Integrity Check - Vinari Software</oldsource>
        <translation>Acerca de Integrity Check - Vinari Software</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="278"/>
        <location filename="src/mainwindow.cpp" line="95"/>
        <source>About Vinari Software</source>
        <translation>Acerca de  Vinari Software</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="287"/>
        <location filename="src/mainwindow.cpp" line="107"/>
        <source>About Vinari OS</source>
        <translation>Acerca de Vinari OS</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="296"/>
        <source>About QT</source>
        <translation>Acerca de QT</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="305"/>
        <source>Quit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="308"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="90"/>
        <source>Integrity Check - Vinari Software | About QT</source>
        <translation>Integrity Check - Vinari Software | Acerca de QT</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="95"/>
        <source>You will be redirected to the Vinari Software website 
Do you want to proceed?</source>
        <translation>Será redirigido al sitio web de Vinari Software ¿Desea continuar?</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="107"/>
        <source>You will be redirected to the Vinari OS website 
Do you want to proceed?</source>
        <translation>Será redirigido al sitio web de Vinari OS ¿Desea continuar?</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="119"/>
        <source>Getting hashing sum, please wait...</source>
        <translation>Obteniendo suma de hash, por favor, espere...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="57"/>
        <location filename="src/mainwindow.cpp" line="160"/>
        <source>Done.</source>
        <translation>Listo.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="125"/>
        <source>Choose a file...</source>
        <translation>Seleccione un archivo...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="140"/>
        <location filename="src/mainwindow.cpp" line="145"/>
        <source>File: </source>
        <translation>Archivo: </translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="156"/>
        <source>The file </source>
        <translation>El archivo </translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="156"/>
        <source> is not accessible.
Do you have enough permissions?</source>
        <translation> no es accesible.
¿Tiene los permisos suficientes?</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="185"/>
        <source>One of the boxes is empty.
It is not possible to do a comparison.</source>
        <translation>Uno de los recuardros está vacío.
No es posible realizar una comparación.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="206"/>
        <source>The hashing sums match completly!.
The file is ok to use.</source>
        <translation>¡Las sumas hash coinciden perfectamente!
El archivo es seguro para su uso.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="210"/>
        <source>The hashing sums do not match.
It is possible that the file is altered, corrupt or incomplete.</source>
        <translation>Las sumas de hash no coinciden.
Es posible que el archivo esté alterado, corrupto o incompleto.</translation>
    </message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <location filename="ui/settingswindow.ui" line="38"/>
        <source>Integrity Check - Vinari Software | Preferences</source>
        <translation>Integrity Check - Vinari Software | Preferencias</translation>
    </message>
    <message>
        <location filename="ui/settingswindow.ui" line="60"/>
        <source>Ignore uppercase and lowercase difference.</source>
        <translation>Ignorar diferencia entre mayúsculas y minúsculas.</translation>
    </message>
    <message>
        <location filename="ui/settingswindow.ui" line="87"/>
        <source>Compare when the two fields have content.</source>
        <translation>Comparar cuando ambos campos tengan contenido.</translation>
    </message>
    <message>
        <location filename="ui/settingswindow.ui" line="121"/>
        <source>Select the verification sum you want to calculate.</source>
        <translation>Seleccione la suma de verificación que desea calcular.</translation>
    </message>
    <message>
        <location filename="ui/settingswindow.ui" line="132"/>
        <source>MD5</source>
        <translation>MD5</translation>
    </message>
    <message>
        <location filename="ui/settingswindow.ui" line="137"/>
        <source>SHA1</source>
        <translation>SHA1</translation>
    </message>
    <message>
        <location filename="ui/settingswindow.ui" line="142"/>
        <source>SHA256</source>
        <translation>SHA256</translation>
    </message>
    <message>
        <location filename="ui/settingswindow.ui" line="147"/>
        <source>SHA384</source>
        <translatorcomment>SHA384</translatorcomment>
        <translation>SHA384</translation>
    </message>
    <message>
        <location filename="ui/settingswindow.ui" line="152"/>
        <source>SHA512</source>
        <translation>SHA512</translation>
    </message>
    <message>
        <location filename="ui/settingswindow.ui" line="164"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="ui/settingswindow.ui" line="177"/>
        <source>Apply changes</source>
        <translation>Aplicar cambios</translation>
    </message>
</context>
</TS>
