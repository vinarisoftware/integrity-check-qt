/*
    Copyright (c) 2015 - 2025, Vinari Software
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	   contributors may be used to endorse or promote products derived from
	   this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include "lib/mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::loadSettings(){
    QSettings savedSettings("org.vinarisoftware", "integritycheck");
	savedSettings.beginGroup("IntegrityCheck.MainWindow");

	QRect defaultPos(250, 250, 340, 400);
	QRect ICRect=savedSettings.value("position", defaultPos).toRect();
	setGeometry(ICRect);

    this->selectedVerificationSum=savedSettings.value("verificationSum", 7894).toInt();
    this->ignoreCapitalizationFlag=savedSettings.value("ignoreCapitalization", true).toBool();
    this->automaticComparisonFlag=savedSettings.value("autoComparison", false).toBool();
	savedSettings.endGroup();
}

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);
    this->statusBar()->setSizeGripEnabled(false);
	loadSettings();
    this->clipboard = QGuiApplication::clipboard();
    ui->statusbar->showMessage(tr("Done."), 2000);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::updateSettings(){
    QSettings savedSettings("org.vinarisoftware", "integritycheck");
    savedSettings.beginGroup("IntegrityCheck.MainWindow");

    this->selectedVerificationSum=savedSettings.value("verificationSum", 7894).toInt();
    this->ignoreCapitalizationFlag=savedSettings.value("ignoreCapitalization", true).toBool();
    this->automaticComparisonFlag=savedSettings.value("autoComparison", false).toBool();
    savedSettings.endGroup();
}

void MainWindow::saveSettings(){
    QSettings settingsToSave("org.vinarisoftware", "integritycheck");
    settingsToSave.beginGroup("IntegrityCheck.MainWindow");
    settingsToSave.setValue("position", this->geometry());

    settingsToSave.endGroup();
}

void MainWindow::closeEvent(QCloseEvent *event){
	saveSettings();
	event->accept();
}

void MainWindow::on_actionAcerca_de_QT_triggered()
{
    QMessageBox::aboutQt(this, tr("Integrity Check - Vinari Software | About QT"));
}

void MainWindow::on_actionAcerca_de_Vinari_Software_triggered()
{
    int questionReply=QMessageBox::question(this, tr("About Vinari Software"), tr("You will be redirected to the Vinari Software website \nDo you want to proceed?"), QMessageBox::Yes|QMessageBox::No);

	if(questionReply==QMessageBox::Yes){
		QDesktopServices::openUrl(QUrl("https://vinarisoftware.wixsite.com/vinari"));
	}
	else{
		return;
	}
}

void MainWindow::on_actionAcerca_de_Vinari_OS_triggered()
{
    int questionReply=QMessageBox::question(this, tr("About Vinari OS"), tr("You will be redirected to the Vinari OS website \nDo you want to proceed?"), QMessageBox::Yes|QMessageBox::No);

	if(questionReply==QMessageBox::Yes){
        QDesktopServices::openUrl(QUrl("https://vinarios.me"));
	}
	else{
		return;
	}
}

void MainWindow::on_actionCalcular_suma_criptografica_de_un_archivo_triggered()
{
    ui->statusbar->showMessage(tr("Getting hashing sum, please wait..."));
    this->updateSettings();

    QString fileFilter="All files (*.*)";
	QString fileNameSelected="";

    fileNameSelected=QFileDialog::getOpenFileName(this, tr("Choose a file..."), QDir::homePath(), fileFilter);

	if(fileNameSelected==""){
		return;
	}

	QFile fileSelected(fileNameSelected);
    QFileInfo fileInfo(fileSelected.fileName());

	if(fileSelected.open(QFile::ReadOnly)){
        myHash=new HashFunctions(this->selectedVerificationSum);
        QString fileHash=myHash->getHash(fileSelected);

        if(ui->textEdit->toPlainText().isEmpty()){
            ui->textEdit->setText(fileHash);
            ui->label_2->setText(tr("File: ") + fileInfo.fileName());
            ui->textEdit->setReadOnly(true);
        }else if(ui->textEdit_2->toPlainText().isEmpty()){
            ui->actionCalcular_suma_criptografica_de_un_archivo->setEnabled(false);
            ui->textEdit_2->setText(fileHash);
            ui->label_3->setText(tr("File: ") + fileInfo.fileName());
            ui->textEdit_2->setReadOnly(true);

            if(this->automaticComparisonFlag){
                this->on_pushButton_clicked();
            }
        }

        delete(myHash);
		fileSelected.close();
	}else{
        QMessageBox::warning(this, "Integrity Check - Vinari Software", tr("The file ")+fileNameSelected+tr(" is not accessible.\nDo you have enough permissions?"));
		return;
	}

    ui->statusbar->showMessage(tr("Done."), 2500);
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->label_2->setText(tr("Type the verification sum you got from the file:"));
    ui->textEdit->setReadOnly(false);
    ui->label_3->setText(tr("Type the verification sum of the file that should be:"));
    ui->textEdit_2->setReadOnly(false);
	ui->pushButton->setEnabled(true);
	ui->actionCalcular_suma_criptografica_de_un_archivo->setEnabled(true);

    ui->textEdit->clear();
    ui->textEdit_2->clear();
	ui->label->clear();
}

void MainWindow::on_actionSalir_triggered()
{
	QApplication::quit();
}

void MainWindow::on_pushButton_clicked()
{
    if(ui->textEdit->toPlainText()=="" || ui->textEdit_2->toPlainText()==""){
        QMessageBox::warning(this, "Integrity Check - Vinari Software", tr("One of the boxes is empty.\nIt is not possible to do a comparison."), QMessageBox::Ok);
		return;
	}

    ui->textEdit->setReadOnly(true);
    ui->textEdit_2->setReadOnly(true);
	ui->pushButton->setEnabled(false);
	ui->actionCalcular_suma_criptografica_de_un_archivo->setEnabled(false);

    QString sumONE=ui->textEdit->toPlainText();
    QString sumTWO=ui->textEdit_2->toPlainText();

    this->updateSettings();
    if(this->ignoreCapitalizationFlag){
		sumONE=sumONE.toLower();
		sumTWO=sumTWO.toLower();
	}

	if(sumONE==sumTWO){
		QPixmap iconOK(":/Resources/OK.png");
		ui->label->setPixmap(iconOK);
        QMessageBox::information(this, "Integrity Check - Vinari Software", tr("The hashing sums match completly!.\nThe file is ok to use."), QMessageBox::Ok);
	}else{
		QPixmap iconFAIL(":/Resources/FAIL.png");
		ui->label->setPixmap(iconFAIL);
        QMessageBox::warning(this, "Integrity Check - Vinari Software", tr("The hashing sums do not match.\nIt is possible that the file is altered, corrupt or incomplete."), QMessageBox::Ok);
	}
}

void MainWindow::on_actionAcerca_de_Integrity_Check_Vinari_Software_triggered()
{
	aboutWindow=new AboutWindow(this);
	aboutWindow->show();
}

void MainWindow::on_actionOpciones_triggered()
{
	settingsWindow=new SettingsWindow(this);
	settingsWindow->show();
}

void MainWindow::on_pushButton_3_clicked()
{
    this->clipboard->setText(ui->textEdit->toPlainText());
}


void MainWindow::on_pushButton_4_clicked()
{
    this->clipboard->setText(ui->textEdit->toPlainText());
}
